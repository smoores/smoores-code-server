FROM codercom/code-server

USER root

ENV ODCVERSION=0.1.2
RUN sudo curl -fsSL -o /usr/local/bin/odc https://gitlab.com/api/v4/projects/33929624/packages/generic/odc/${ODCVERSION}/linux
RUN sudo chmod +x /usr/local/bin/odc && sudo chown coder /usr/local/bin/odc

ENV DOCKERVERSION=20.10.9
RUN curl -fsSLO https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKERVERSION}.tgz \
  && tar xzvf docker-${DOCKERVERSION}.tgz --strip 1 \
                 -C /usr/local/bin docker/docker \
  && rm docker-${DOCKERVERSION}.tgz

USER coder

ENTRYPOINT ["/usr/bin/entrypoint.sh", "--bind-addr", "0.0.0.0:8080", "."]
